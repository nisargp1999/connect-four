# Connect-Four

This is an implementation of a GUI and gameplay for a very popular two-player game, Connect Four. 
Each of the players has either red or green colored disks that he/she will drop down into 7 column by 6 row grid. 
The initial discs will go to the bottom. 
A disc may only be placed either onto the bottom or on top of another disc (or either color).

To run the program:

1. Clone or download a zip file of this project
2. Unzip the file / open the Connect-Four folder
3. Open the .jar file
4. Enjoy the show
