/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
/**
 *
 * @author Nisarg
 */
public class FXMLDocumentController implements Initializable {
    
    private boolean p1 = true;
    private boolean connected = false;
    @FXML
    private GridPane theGrid;
    private ImageView winView;
    private Image winImage;
    
    private ImageView redColor;
    private Image redImage;
    
    private ImageView greenColor;
    private Image greenImage;
    
    private ImageView gameOverColor;
    private Image gameOverImage;
    
    int[][] gridArray = new int[7][6];

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        redImage = new Image("images/redCircle_1.png");
        greenImage = new Image("images/greenCircle_1.png");
        
        redColor = new ImageView(redImage);
        greenColor = new ImageView(greenImage);
        
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                gridArray[i][j] = 0;
            }
        }
    }    
    
    @FXML
    private boolean allFill()
    {
        boolean result = true;
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (gridArray[i][j] == 0){
                    result = false;
                }
            }
        }
        if (result)
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
        return result;
    }
    
    @FXML
    private void buttonPress(int c)
    {
        if (!allFill())   
        {
        int r = 5;
        while (gridArray[c][r] != 0)
        {
            r--;
        }
        if (p1)
        {
            greenImage = new Image("images/greenCircle_1.png");
            greenColor = new ImageView(greenImage);
            theGrid.add(greenColor, c, r);
            gridArray[c][r] = 1;
            p1 = false;
        } else {
            redImage = new Image("images/redCircle_1.png");
            redColor = new ImageView(redImage);
            theGrid.add(redColor, c, r);
            gridArray[c][r] = 2;
            p1 = true;
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
        }
    }
    
    @FXML
    private void button0(ActionEvent event) {
        if (!allFill())
        {
          buttonPress(0);
          if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
          if (connected)
          {
            winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
            winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
        
    }
    
    @FXML
    private void button1(ActionEvent event)
    {
        if (!allFill()){
        buttonPress(1);
        if (p1){
            connected = areFourConnected(2);
        }
        else
            connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
        
    }
    
       @FXML
    private void button2(ActionEvent event)
    {
        if (!allFill())
        {
        buttonPress(2);
        if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
    }
    
       @FXML
    private void button3(ActionEvent event)
    {
        if (!allFill())
        {
        buttonPress(3);
        if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
    }
    
       @FXML
    private void button4(ActionEvent event)
    {
        if (!allFill())
        {
        buttonPress(4);
        if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
    }
    
       @FXML
    private void button5(ActionEvent event)
    {
        if (!allFill())
        {
        buttonPress(5);
        if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
    }
    
    @FXML
    private void button6(ActionEvent event)
    {
        if (!allFill())
        {
        buttonPress(6);
        if (p1)
            connected = areFourConnected(2);
          else
              connected = areFourConnected(1);
        if (connected)
          {
              winImage = new Image("images/win.png");
            winView = new ImageView(winImage);
            theGrid.add(winView, 3, 2);
          }
        }
        if (allFill())
        {
            gameOverImage = new Image("images/gameOver.png");
            gameOverColor = new ImageView(gameOverImage);
            theGrid.add(gameOverColor, 3, 2);
        }
    }
    
    @FXML
   public boolean areFourConnected(int player){

    // verticalCheck 
    for (int j = 0; j<3 ; j++ ){
        for (int i = 0; i<7; i++){
            if (gridArray[i][j] == player && gridArray[i][j+1] == player && gridArray[i][j+2] == player && gridArray[i][j+3] == player){
                return true;
            }           
        }
    }
    // horizontalCheck
    for (int i = 0; i<4 ; i++ ){
        for (int j = 0; j<6; j++){
            if (gridArray[i][j] == player && gridArray[i+1][j] == player && gridArray[i+2][j] == player && gridArray[i+3][j] == player){
                return true;
            }           
        }
    }
    // ascendingDiagonalCheck 
    for (int i=3; i<7; i++){
        for (int j=0; j<3; j++){
            if (gridArray[i][j] == player && gridArray[i-1][j+1] == player && gridArray[i-2][j+2] == player && gridArray[i-3][j+3] == player)
                return true;
        }
    }
    // descendingDiagonalCheck
    for (int i=3; i<7; i++){
        for (int j=3; j<6; j++){
            if (gridArray[i][j] == player && gridArray[i-1][j-1] == player && gridArray[i-2][j-2] == player && gridArray[i-3][j-3] == player)
                return true;
        }
    }
    return false;
   }
}
